# Authentication internals

 - One word: `bcrypt`
 - Work factor for salt: 14

## Tokens

 - https://pythonhosted.org/itsdangerous/
 - Uses HMAC-SHA1 internally.
